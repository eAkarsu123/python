import sys

if len(sys.argv) < 2:
    sys.stderr.write("That's not how you do it!\n")
    sys.exit(1)

print(sys.argv[1])

x=0
while x <= len(sys.argv):
    try:
        print(sys.argv[x])
        input("See Something say nothing: ")
        x+=1 #x + 1
    except IndexError:
        print("This was your typical developer issue")
        print("But we handled it :)")
        break
    except KeyboardInterrupt:
        print("Dont do that!")
        break
    except:
        print("Some other error occured")
        print("Cleaning up and exiting...")
        break

print("Successfully reached end of program")
