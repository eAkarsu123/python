import sys

print("Hello World")

myname=input("Enter your name:")
print ("Hello" +myname)

print ("Command line arge: "+str(sys.argv))
#print(sys.argv[1]) #Asks for 1 element in the array

# Using files & Loops & Conditions
peopleFH=open("data.txt","r") #'r' means read, 'w' write, 'a' append
for person in peopleFH: #FH File Handler
    #print(person) #print auto places a new line
    #sys.stdout.write(person)
    #print(person.rstrip('\r\n')) #removes new line spacing
    fields=person.split(',') #tells where to split the line, in this case split from comma
    fields[-1]=fields[-1].rstrip("\r\n") #-1 get the last element of the array
    #print ("Number of elements: "+ste(len(fields))+"\tLast element is: "+str(len(fields)-1)) #Must convert other data type when concatenating with a string
    #if "22" in fields:
    if fields[1] == "22":
        print(type(fields[1])) #determine the type of data stored in variabe
        #print("New age: "+str(int(fields[1])+1)) #add numbers
        print("Name: "+fields[0]+"\t"+"Phone: "+fields[-1]) #first element in array 0, (-) starts reading from right to left
    #print("Name: "+fields[0]+"\t"+"Phone: "+fields[len(fields)-1]) #len(array) number of fields in array

peopleFH.close()

# Regular Exressions - see refExpressions.py
