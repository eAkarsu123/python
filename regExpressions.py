import re ##re is regular expression package

peopleFH=open("data.txt", "r")
ptn=re.compile("^.*ati. ", re.IGNORECASE) #inumerated types.

for person in peopleFH:
    #if re.search("22",person):
    if ptn.search(person):
        print(person.rstrip("\r\n"))

peopleFH.seek(0.0) #goes back to the start of the file

for person in peopleFH:
    #matched=re.search("^.*ati. ",person)
    matched=ptn.search(person)
    if matched:
        print("Matched at",matched.span()) #Locations where match found
        print("Words found: ", matched.group()) #What I found
